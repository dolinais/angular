import { TemplateRef, ElementRef, Component, ViewChild, ContentChild, OnInit } from '@angular/core'
import { Title } from "@angular/platform-browser"
import { Router, ActivatedRoute, Params } from '@angular/router'

import { HttpService } from "../services/http.service"

@Component({
    selector: 'app-landingpage',
    templateUrl: './page.component.html'
})


export class PageComponent  implements OnInit {

    li:any

    isEdit:boolean = false

    constructor(private service : HttpService, private titleService:Title,
        private router: Router, private route: ActivatedRoute, private elRef:ElementRef) {
        // this.titleService.setTitle("Статьи")
    }

    public balance = 0

    public id = this.route.snapshot.params['id']

    public edited = true

    public jsonData()  {
        if (typeof this.id === 'undefined') {
            this.id = 0
        }

        return {
            "url": "api/service",
            "order_id": this.id,
            "params": {
                "type": {
                    "class": "Post",
                    "method": "info"
                },
                "values": {
                    "limit": 10
                }
            }
        }
    }

    public Data()  {

        setInterval(() => this.edited = false, 1000)
        this.service.RequstPOST(this.jsonData())
        .subscribe(Response => {
            this.edited = true
            this.li = Response
            this.balance = this.li.status
            this.li = this.li.output
            this.titleService.setTitle(this.li[0].title)
        }, error => {
            if(error.status == 401) {
              localStorage.removeItem('token')
              localStorage.removeItem('access_token')
              this.router.navigate(['/login'])
            }
            console.log(error.status)
        })
    }

    ngOnInit() {
        if(!localStorage.getItem('token')) {
            this.router.navigate(['/login'])
        }
        this.Data()

    }

    savePost(){

        let loader = this.elRef.nativeElement.querySelector('#loader')
        loader.style.display = "block"

        setTimeout(() =>
          loader.style.display = "none"
        , 1000)

        this.isEdit = !this.isEdit
        this.service.RequstPOST({
            "url": "api/articles",
            "order_id": 2
        })
        // .subscribe(Response => {
        //     this.edited = true
        //     this.li = Response
        //     this.balance = this.li.status
        //     this.li = this.li.output
        // }, error => {
        //     if(error.status == 401) {
        //       localStorage.removeItem('token')
        //       localStorage.removeItem('access_token')
        //       this.router.navigate(['/login'])
        //     }
        //     console.log(error.status)
        // })
    }
}