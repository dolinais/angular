import { Component, ViewChild, OnInit, ElementRef } from '@angular/core'
import { Title } from "@angular/platform-browser"
import { Router, ActivatedRoute, Params } from '@angular/router'

import { HttpService } from "../services/http.service"

@Component({
    selector: 'app-landingpage',
    templateUrl: './landingpage.component.html'
})


export class LandingpageComponent  implements OnInit {

    li:any

    constructor(private service : HttpService, private titleService:Title,
        private router: Router, private elRef:ElementRef) {
        this.titleService.setTitle("Магазин")
    }

    public balance = 0

    public transform(value: number): string {
        if (value !== undefined && value !== null) {
          return new Intl.NumberFormat('ru-RU').format(value);
        } else {
          return '';
        }
    }

    public jsonData()  {

        return {
            "url": "api/service",
            "params": {
                "type": {
                    "class": "Post",
                    "method": "info"
                },
                "values": {
                    "limit": 10
                }
            }
        }
    }

    public Data()  {

        this.service.RequstPOST(this.jsonData())
        .subscribe(Response => {
            this.li = Response
            this.balance = 250
            this.li = this.li.output
        }, error => {
            if(error.status == 401) {
              localStorage.removeItem('token')
              localStorage.removeItem('access_token')
              this.router.navigate(['/login'])
            }
            console.log(error.status)
        })
    }

    ngOnInit() {

        if(!localStorage.getItem('token')) {
            this.router.navigate(['/login'])
        }

        this.Data()
    }

}