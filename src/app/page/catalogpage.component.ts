import { Component, ViewChild, OnInit, ElementRef } from '@angular/core'
import { Title } from "@angular/platform-browser"
import { Router, ActivatedRoute, Params } from '@angular/router'

import { HttpService } from "../services/http.service"

@Component({
    selector: 'app-landingpage',
    templateUrl: './catalogpage.component.html'
})


export class CatalogpageComponent  implements OnInit {

    li:any
    pagination:any

    constructor(private service : HttpService, private titleService:Title,
        private router: Router, private elRef:ElementRef) {
        this.titleService.setTitle("Каталог")
    }

    public balance = 0

    public transform(value: number): string {
        if (value !== undefined && value !== null) {
          return new Intl.NumberFormat('ru-RU').format(value);
        } else {
          return '';
        }
    }

    public jsonData()  {

        return {
            "url": "api/service",
            "params": {
                "type": {
                    "class": "Post",
                    "method": "info"
                },
                "values": {
                    "limit": 10
                }
            }
        }
    }

    public Data()  {

        this.service.RequstPOST(this.jsonData())
        .subscribe(Response => {
            this.li = Response
            this.balance = 250
            this.li = this.li.output
        }, error => {
            console.log(error.status)
        })
    }

    getCookie(name:string) {
        var matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    public CloseCatalog(){
        document.cookie = "Catalogviews=0"
        this.Catalogviews(0)
    }

    public OpenCatalog(){
        document.cookie = "Catalogviews=1"
        this.Catalogviews(1)
    }

    public Catalogviews(catalogparams:number){
        if(catalogparams == 1){
            let opencatalog = this.elRef.nativeElement.querySelector('#catalogviews-open')
            let closecatalog = this.elRef.nativeElement.querySelector('#catalogviews-close')
            closecatalog.style.display = "none"
            opencatalog.style.display = "block"
        }
        if(catalogparams == 0){
            let opencatalog = this.elRef.nativeElement.querySelector('#catalogviews-open')
            let closecatalog = this.elRef.nativeElement.querySelector('#catalogviews-close')
            closecatalog.style.display = "block"
            opencatalog.style.display = "none"
        }
    }

    ngOnInit() {

        if(!localStorage.getItem('token')) {
            this.router.navigate(['/login'])
        }

        if(this.getCookie('Catalogviews') == '1'){
            this.Catalogviews(1)
        }
        if(this.getCookie('Catalogviews') == '0'){
            this.Catalogviews(0)
        }

        this.Data()
    }
}