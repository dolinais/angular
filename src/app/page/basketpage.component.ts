import { Component, ViewChild, OnInit, ElementRef } from '@angular/core'
import { Title } from "@angular/platform-browser"
import { Router, ActivatedRoute, Params } from '@angular/router'
import { SearchService } from '../services/search.service'
import { HttpService } from "../services/http.service"
import { NgForm, FormGroup, FormControl, Validators } from '@angular/forms'

@Component({
    selector: 'app-landingpage',
    templateUrl: './basketpage.component.html'
})


export class BasketpageComponent  implements OnInit {

    li:any
    employeesTasks: any
    form!: FormGroup;
    public now: Date = new Date()
    public price: any
    public pcount: any
    public balance = 0
    public id = this.route.snapshot.params['id']

    constructor(private service : HttpService, private titleService:Title,
        private router: Router, private route: ActivatedRoute, private elRef:ElementRef, private search : SearchService) {
        this.titleService.setTitle("Касса 1.0")
        this.search.Data().subscribe((data:any) => {
            this.employeesTasks = data.output
        })
        setInterval(() => {
          this.now = new Date();
        }, 1000);
    }

    public transform(value: number): string {
        if (value !== undefined && value !== null) {
          return new Intl.NumberFormat('ru-RU').format(value);
        } else {
          return '';
        }
    }

    public getRandomNumber(min: number, max: number) {
        return Math.floor(Math.random() * (max - min + 1)) + min
        // alert(this.getRandomNumber(1,4));
    }

    public jsonData()  {
        return {
            "url": "api/product",
            "params": {
                "type": {
                    "class": "Basket",
                    "method": "info"
                },
                "values": {
                }
            }
        }
    }

    public UpdateCountJsonData(value: any)  {
        return {
            "url": "api/product",
            "params": {
                "type": {
                    "class": "Basket",
                    "method": "update"
                },
                "values": {
                    "id": value.id,
                    "product_count": value.value
                }
            }
        }
    }

    public AddJsonData(value: any)  {
        return {
            "url": "api/product",
            "params": {
                "type": {
                    "class": "Basket",
                    "method": "create"
                },
                "values": {
                    "product_id": value.id,
                    "product_name": value.title,
                    "product_count": 1,
                    "price": value.price
                }
            }
        }
    }

    public DelJsonData(id: number)  {
        return {
            "url": "api/product",
            "params": {
                "type": {
                    "class": "Basket",
                    "method": "delete"
                },
                "values": {
                    "id": id
                }
            }
        }
    }

    public OrderJsonData(id: number)  {
        return {
            "url": "api/product",
            "params": {
                "type": {
                    "class": "Order",
                    "method": "create"
                },
                "values": {
                    "order_status_id": 1
                }
            }
        }
    }

    public CountProduct(value: any) {
        this.service.RequstPOST(this.UpdateCountJsonData(value.target))
        .subscribe(Response => {
            this.Data()
        }, error => {
            console.log(error.status)
        })
    }

    public addproduct(value: any) {
        if(value.itemData.id){
            this.service.RequstPOST(this.AddJsonData(value.itemData))
            .subscribe(Response => {
                this.Data()
            }, error => {
                console.log(error.status)
            })
        }
    }

    public DelProduct(value: number): void {
        this.service.RequstPOST(this.DelJsonData(value))
        .subscribe(Response => {
            this.Data()
        }, error => {
            console.log(error.status)
        })
    }

    public Data()  {
        this.service.RequstPOST(this.jsonData())
        .subscribe(Response => {
            this.li = Response
            this.balance = 250
            this.li = this.li.output
            let pcount = 0;
            let price = 0;
            for (var item of this.li) {
                pcount += item.product_count
            }
            for (var itemprice of this.li) {
                price += Math.floor(itemprice.total_price)
            }
            this.pcount = pcount
            this.price = price
        }, error => {
            console.log(error.status)
        })
    }

    public Order(){
        this.service.RequstPOST(this.OrderJsonData(this.id))
        .subscribe(Response => {
            this.li = Response
            this.li = this.li.output
            alert(this.li)
            this.Data()
        }, error => {
            console.log(error.status)
        })
    }

    ngOnInit() {

        if(!localStorage.getItem('token')) {
            this.router.navigate(['/login'])
        }

        this.Data()
    }
}