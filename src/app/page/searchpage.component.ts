import { Component, ViewChild, OnInit, ElementRef } from '@angular/core'
import { Title } from "@angular/platform-browser"
import { Router, ActivatedRoute, Params } from '@angular/router'
import { SearchService } from '../services/search.service'
import { HttpService } from "../services/http.service"
import { NgForm, FormGroup, FormControl, Validators } from '@angular/forms'

@Component({
    selector: 'app-landingpage',
    templateUrl: './searchpage.component.html'
})


export class SearchpageComponent  implements OnInit {

    li:any
    employeesTasks: any
    public now: Date = new Date()
    name = new FormControl(this.count());
    form!: FormGroup;
    public price: any
    public pcount: any

    constructor(private service : HttpService, private titleService:Title,
        private router: Router, private elRef:ElementRef, private search : SearchService) {
        this.titleService.setTitle("Корзина")
        this.search.Data().subscribe((data:any) => {
            this.employeesTasks = data.output
        })
        setInterval(() => {
          this.now = new Date();
        }, 1);
    }

    public balance = 0
    count () {
        return 1
    }

    public jsonData()  {

        return {
            "url": "api/product",
            "params": {
                "type": {
                    "class": "Basket",
                    "method": "info"
                },
                "values": {
                    "id": 1
                }
            }
        }
    }
    public AddJsonData(id: string, value: string)  {
        // alert(id.replace('product_count',''))
        return {
            "url": "api/product",
            "params": {
                "type": {
                    "class": "Basket",
                    "method": "update"
                },
                "values": {
                    "id": id.replace('product_count',''),
                    "product_id": 1,
                    "product_count": value,
                    "price": 60000
                }
            }
        }
    }
    DelProduct(value: number): void {
        alert(value)
    }
    saveData (f: NgForm) {

        // this.name.setValue()
        // alert (f.value.product_count_1)
        // console.log (this.li)

        let form = f.value
        for (var key in form) {
           console.log(key+'-'+form[key])
           this.service.RequstPOST(this.AddJsonData(key, form[key]))
            .subscribe(Response => {
                this.li = Response
                this.li = this.li.output
            }, error => {
                console.log(error.status)
            })
        }
        this.Data()

        // window.location.reload()
        let data = JSON.stringify(form);
        var values = [];
        JSON.parse(data, function (key, value) {

            if (typeof(value) != "object") {
                // console.log(values.push({[key]:value}))
            // values.push(value); //if you need a value array
            }
        });
    }
    public transform(value: number): string {
        if (value !== undefined && value !== null) {
            // value.toString().replace(/,/g, '')
          // return value.toLocaleString('ru-RU');
          return new Intl.NumberFormat('ru-RU').format(value);
        } else {
          return '';
        }
    }
    public Data()  {

        this.service.RequstPOST(this.jsonData())
        .subscribe(Response => {
            this.li = Response
            this.balance = 250
            this.li = this.li.output
            let pcount = 0;
            let price = 0;
            for (var item of this.li) {
                pcount += item.product_count
            }
            for (var itemprice of this.li) {
                price += Math.floor(itemprice.total_price)
            }
            console.log(price)
            this.pcount = pcount
            this.price = price

            var countDecimals = function (value:number) {
            if(Math.floor(value) === value) return 0;
                return value.toString().split(".")[1].length || 0;
            }

            // console.log(countDecimals(price))

        }, error => {
            if(error.status == 401) {
              localStorage.removeItem('token')
              localStorage.removeItem('access_token')
              this.router.navigate(['/login'])
            }
            console.log(error.status)
        })
    }

    ngOnInit() {

        if(!localStorage.getItem('token')) {
            this.router.navigate(['/login'])
        }

        this.Data()
    }

}