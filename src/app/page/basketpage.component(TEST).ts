import { Component, ViewChild, OnInit, ElementRef } from '@angular/core'
import { Title } from "@angular/platform-browser"
import { Router, ActivatedRoute, Params } from '@angular/router'
import { SearchService } from '../services/search.service'
import { HttpService } from "../services/http.service"
import { NgForm, FormGroup, FormControl, Validators } from '@angular/forms'

@Component({
    selector: 'app-landingpage',
    templateUrl: './basketpage.component.html'
})


export class BasketpageComponent  implements OnInit {

    li:any
    employeesTasks: any
    form!: FormGroup;
    public now: Date = new Date()
    public price: any
    public pcount: any
    public balance = 0

    constructor(private service : HttpService, private titleService:Title,
        private router: Router, private elRef:ElementRef, private search : SearchService) {
        this.titleService.setTitle("Корзина")
        this.search.Data().subscribe((data:any) => {
            this.employeesTasks = data.output
        })
        setInterval(() => {
          this.now = new Date();
        }, 1);
    }

    public transform(value: number): string {
        if (value !== undefined && value !== null) {
          return new Intl.NumberFormat('ru-RU').format(value);
        } else {
          return '';
        }
    }

    public jsonData()  {

        return {
            "url": "api/product",
            "params": {
                "type": {
                    "class": "Basket",
                    "method": "info"
                },
                "values": {
                    "id": 1
                }
            }
        }
    }

    public UpdateJsonData(id: string, value: string)  {
        return {
            "url": "api/product",
            "params": {
                "type": {
                    "class": "Basket",
                    "method": "update"
                },
                "values": {
                    "id": id.replace('product_count',''),
                    "product_id": 1,
                    "product_count": value,
                    "price": 60000
                }
            }
        }
    }

    public UpdateCountJsonData(value: any)  {
        return {
            "url": "api/product",
            "params": {
                "type": {
                    "class": "Basket",
                    "method": "update"
                },
                "values": {
                    "id": value.id,
                    "product_id": 1,
                    "product_count": value.value,
                    "price": 60000
                }
            }
        }
    }

    public AddJsonData(value: any)  {
        console.log (value)
        return {
            "url": "api/product",
            "params": {
                "type": {
                    "class": "Basket",
                    "method": "create"
                },
                "values": {
                    "basket_id": 1,
                    "user_id": 1,
                    "product_id": value.id,
                    "product_name": value.title,
                    "product_count": 1,
                    "price": value.price
                }
            }
        }
    }

    public DelJsonData(id: number)  {
        return {
            "url": "api/product",
            "params": {
                "type": {
                    "class": "Basket",
                    "method": "delete"
                },
                "values": {
                    "id": id
                }
            }
        }
    }

    public CountProduct(value: any) {
        console.log (value.target.id)
        this.service.RequstPOST(this.UpdateCountJsonData(value.target))
        .subscribe(Response => {
            this.Data()
        }, error => {
            console.log(error.status)
        })
    }

    public addproduct(value: any) {
        if(value.itemData.id){
            this.service.RequstPOST(this.AddJsonData(value.itemData))
            .subscribe(Response => {
                this.li = Response
                this.li = this.li.output
                this.Data()
            }, error => {
                console.log(error.status)
            })
        }

        console.log(value.itemData.id)
    }

    public DelProduct(value: number): void {
        this.service.RequstPOST(this.DelJsonData(value))
        .subscribe(Response => {
            this.li = Response
            this.Data()
        }, error => {
            console.log(error.status)
        })
    }

    public saveData (f: NgForm) {

        // let form = f.value
        // for (var key in form) {
        //    console.log(key+'-'+form[key])
        //    this.service.RequstPOST(this.UpdateJsonData(key, form[key]))
        //     .subscribe(Response => {
        //         this.li = Response
        //         this.li = this.li.output
        //         this.Data()
        //     }, error => {
        //         console.log(error.status)
        //     })
        // }
    }

    public Data()  {

        this.service.RequstPOST(this.jsonData())
        .subscribe(Response => {
            this.li = Response
            this.balance = 250
            this.li = this.li.output
            let pcount = 0;
            let price = 0;
            for (var item of this.li) {
                pcount += item.product_count
            }
            for (var itemprice of this.li) {
                price += Math.floor(itemprice.total_price)
            }
            this.pcount = pcount
            this.price = price
        }, error => {
            console.log(error.status)
        })
    }

    ngOnInit() {

        if(!localStorage.getItem('token')) {
            this.router.navigate(['/login'])
        }

        this.Data()
    }

}