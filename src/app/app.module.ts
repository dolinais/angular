import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { HttpClientModule } from '@angular/common/http'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { DxAutocompleteModule } from 'devextreme-angular'

import { AppComponent } from './init/app.component'
import { AppRoutingModule } from './init/app-routing.module'
import { Preloader } from './init/app.preloader'

import { SearchService } from './services/search.service'

import { LoginPageComponent } from './account/login-page.component'
import { AccountComponent } from './account/account.component'

import { LandingpageComponent } from './page/landingpage.component'
import { CatalogpageComponent } from './page/catalogpage.component'
import { PageComponent } from './page/page.component'
import { SearchpageComponent } from './page/searchpage.component'
import { BasketpageComponent } from './page/basketpage.component'

import { SiteLayoutComponent } from './shared/layouts/site-layout/site-layout.component'


@NgModule({
    declarations: [
        AppComponent,
        SiteLayoutComponent,
        CatalogpageComponent,
        LandingpageComponent,
        LoginPageComponent,
        AccountComponent,
        PageComponent,
        SearchpageComponent,
        BasketpageComponent,
        Preloader
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        DxAutocompleteModule
    ],
    providers: [SearchService],
    bootstrap: [AppComponent]
})

export class AppModule { }
