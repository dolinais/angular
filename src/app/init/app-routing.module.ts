import {NgModule} from '@angular/core'
import {RouterModule, Routes} from '@angular/router'
import {SiteLayoutComponent} from '../shared/layouts/site-layout/site-layout.component'
import {LoginPageComponent} from '../account/login-page.component'
import {AccountComponent} from '../account/account.component'
import {LandingpageComponent} from '../page/landingpage.component'
import {CatalogpageComponent} from '../page/catalogpage.component'
import {PageComponent} from '../page/page.component'
import {BasketpageComponent} from '../page/basketpage.component'


const routes: Routes = [
	{
		path: '', component: SiteLayoutComponent, children: [
			{ path: '', component: BasketpageComponent },
			{ path: 'login', component: LoginPageComponent },
			{ path: 'account', component: AccountComponent },
			{ path: 'catalog', component: CatalogpageComponent },
			{ path: 'basket', component: BasketpageComponent },
			{ path: 'basket/:id', component: BasketpageComponent },
			{ path: 'page/:id', component: PageComponent }
		]
	}
]

@NgModule({
	imports: [
		RouterModule.forRoot(routes)
	],
	exports: [
		RouterModule
	]
})

export class AppRoutingModule {}