import { Component, ElementRef } from '@angular/core'
import { Router } from '@angular/router'
import { DomSanitizer, SafeHtml } from '@angular/platform-browser'
import {Preloader} from './app.preloader'

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html'
})

export class AppComponent {

    constructor(private router: Router, private elRef:ElementRef,
        private sanitizer: DomSanitizer
    ) {}

    ngOnInit() {
        if(!localStorage.getItem('token')) {
            this.router.navigate(['/login'])
        }
    }
}
