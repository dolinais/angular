import { Component, ElementRef } from '@angular/core'
import { Router } from '@angular/router'
import { DomSanitizer, SafeHtml } from '@angular/platform-browser'

@Component({
    selector: 'app-preloader',
    templateUrl: './loader.html',
})

export class Preloader {

    constructor(private router: Router, private elRef:ElementRef,
        private sanitizer: DomSanitizer
    ) {}

    ngOnInit() {
        let loader = this.elRef.nativeElement.querySelector('#loader');
        setTimeout(() =>
          loader.style.display = "none"
        , 1000)
    }
}