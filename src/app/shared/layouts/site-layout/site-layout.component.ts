import { Component, ElementRef } from '@angular/core'
import { Router } from '@angular/router'
import { HttpService } from "../../../services/http.service"

@Component({
    selector: 'app-site-layout',
    templateUrl: './site-layout.component.html',
    styleUrls: ['./site-layout.component.css']
})

export class SiteLayoutComponent {
    public balance = 250
    li:any
    public isMActive = 250
    public authService = true



    constructor(private service : HttpService, private router: Router, private elRef:ElementRef) {}

    logout() {
        localStorage.removeItem('token')
        localStorage.removeItem('access_token')
        this.router.navigate(['/login'])
    }

    public isActive(){
        return "active"
    }

    public jsonData()  {

        return {
            "url": "api/service",
            "params": {
                "type": {
                    "class": "User",
                    "method": "info"
                }
            }
        }
    }

    public Data()  {
        this.service.RequstPOST(this.jsonData())
        .subscribe(Response => {
            this.li = Response
            this.balance = this.li.output.balance
        }, error => {
            if(error.status == 401) { }
            console.log(error.status)
        })
    }

    public CloseMenu(){
        document.cookie = "MenuCookie=0"
        this.MenuParams(0)
    }

    public OpenMenu(){
        document.cookie = "MenuCookie=1"
        this.MenuParams(1)
    }

    public MenuParams(params:number){
        if(params == 1){
            let opennav = this.elRef.nativeElement.querySelector('#nav-open')
            let closenav = this.elRef.nativeElement.querySelector('#nav-close')
            closenav.style.display = "none"
            opennav.style.display = "block"
        }
        if(params == 0){
            let opennav = this.elRef.nativeElement.querySelector('#nav-open')
            let closenav = this.elRef.nativeElement.querySelector('#nav-close')
            closenav.style.display = "block"
            opennav.style.display = "none"
        }
    }

    getCookie(name:string) {
        var matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    ngOnInit() {
        // console.log(this.getCookie('MenuCookie'))
        if(this.getCookie('MenuCookie') == '1'){
            this.MenuParams(1)
        }
        if(this.getCookie('MenuCookie') == '0'){
            this.MenuParams(0)
        }
        if(!localStorage.getItem('token')){
            this.authService = false
        }

        this.Data()
    }
}
