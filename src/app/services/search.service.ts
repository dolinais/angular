import { Injectable } from '@angular/core'
import { HttpService } from "../services/http.service"
import {catchError, map, Observable, of} from "rxjs";

@Injectable({
  providedIn: 'root'
})

export class SearchService {

	li:any

	constructor(private service : HttpService) { }

	public jsonData()  {

        return {
            "url": "api/service",
            "params": {
                "type": {
                    "class": "Post",
                    "method": "info"
                },
                "values": {
                    "limit": 10
                }
            }
        }
    }

    public Data()  {

        return this.service.RequstPOST(this.jsonData())
        .pipe(
          map((data) => {
			return data;
         }),
         catchError((err) => {
           console.error(err);
           throw err;
         }
       ))

    }

}