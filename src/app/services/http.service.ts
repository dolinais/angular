import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { LocalService } from './local.service'
import {catchError, map, Observable, of} from "rxjs"

@Injectable({
    providedIn: 'root'
})

export class HttpService {

    constructor(private httpClient: HttpClient, private localStore: LocalService) { }

    private jsonData = 0

    getPosts(json: any){

        if(this.localStore.getData('access_token')) {
            this.jsonData = JSON.parse(this.localStore.getData('access_token')).token
        }
        return this.httpClient.post(
            'api/articles',
            json,
            { headers: new HttpHeaders({
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'GET, PATCH, PUT, POST, DELETE, OPTIONS',
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.jsonData
            })}
        )
    }

    RequstGet(json: any){

        if(this.localStore.getData('access_token')) {
            this.jsonData = JSON.parse(this.localStore.getData('access_token')).token
        }
        return this.httpClient.get(
            json.url,
            json,
        )
    }

    RequstPOST(json: any){

        if(this.localStore.getData('access_token')) {
            this.jsonData = JSON.parse(this.localStore.getData('access_token')).token
        }
        return this.httpClient.post(
            json.url,
            json,
            { headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + this.jsonData
            })}
        )
    }

    RequstPUT(json: any){

        if(this.localStore.getData('access_token')) {
            this.jsonData = JSON.parse(this.localStore.getData('access_token')).token
        }
        return this.httpClient.put(
            json.url,
            json,
            { headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + this.jsonData
            })}
        )
    }
}