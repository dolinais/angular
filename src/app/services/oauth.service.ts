import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LocalService } from './local.service';

@Injectable({
  providedIn: 'root'
})

export class OauthService {

    constructor(private httpClient: HttpClient, private localStore: LocalService) { }

    getPosts(json: any){
        const myHeaders = new HttpHeaders({
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET, PATCH, PUT, POST, DELETE, OPTIONS',
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': 'Basic ' + window.btoa(json.id +':'+ json.password)
        });
        return this.httpClient.post('api/auth', {
            params: {
                id: json.id,
                password: json.password
            },
            observe: 'response'
        },
        { headers: myHeaders }
        )
    }
}