import { Component, OnInit } from '@angular/core'
import { Title } from "@angular/platform-browser"
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { Router } from '@angular/router'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { LocalService } from '../services/local.service'
import { OauthService } from '../services/oauth.service'

@Component({
    selector: 'app-account',
    templateUrl: './login-page.component.html',
    styleUrls: ['./login-page.component.css']
})

export class LoginPageComponent implements OnInit {

    form!: FormGroup;
    li:any;

    constructor(private titleService:Title, private service : OauthService, private router: Router, private localStore: LocalService, private httpClient: HttpClient) {
        this.titleService.setTitle("Личный кабинет")
    }

    ngOnInit() {
        if(localStorage.getItem('token')){
            this.router.navigate(['/']);
        }

        this.form = new FormGroup({
            email: new FormControl('', [Validators.required]),
            password: new FormControl('', [Validators.required, Validators.minLength(6)]),
        })
    }

    public jsonData()  {
        return {"id": +this.form.value.email, "password":this.form.value.password}
    }

    onSubmit() {
        this.service.getPosts(this.jsonData())
        .subscribe(Response => {
            this.li = Response
            if(this.li.status == 200) {
                this.localStore.saveData('token', "1")
                this.localStore.saveData('access_token', '{"token": "'+this.li.accessToken+'", "date":"'+new Date().getTime()+'"}')
            }
            if(this.li.status == 401) {
                alert('Ошибка авторизации')
            }
            window.location.reload()
        })
    }
}
