import { TemplateRef, ElementRef, Component, ViewChild, ContentChild, OnInit } from '@angular/core'
import { Title } from "@angular/platform-browser"
import { SearchService } from '../services/search.service'

@Component({
    selector: 'app-account',
    templateUrl: './account.component.html',
})

export class AccountComponent implements OnInit {

    employeesTasks: any

    constructor(private titleService:Title,
        private search : SearchService) {
        this.titleService.setTitle("Личный кабинет")
        this.search.Data().subscribe((data:any) => {
            this.employeesTasks = data.output
        })
    }

    ngOnInit() { }
}
